import i18n from 'src/common/lib/i18n';

const errorTypeError = (e: TypeError): string => {
    return `Message: ${e.message}\nStack: ${e.stack}`;
};

export const errorToString = (e: any): string => {
    if(e instanceof TypeError) {
        return errorTypeError(e);
    } else if(e.translatedMessageKey) {
        return i18n.t(e.translatedMessageKey);
    } else {
        return JSON.stringify(e);
    }
};