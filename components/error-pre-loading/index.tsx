import React from 'react';
import { Helmet } from 'react-helmet';

import { errorToString } from 'src/common/components/error-pre-loading/utils';
import i18n from 'src/common/lib/i18n';

interface Props {
    error: any;
}

/**
 * Shown when the app cannot load due to a backend error. It simply shows an error page.
 */
export default ({ error }: Props) => (
    <div className="app">

        <Helmet titleTemplate={ `%s - ${i18n.t(`backend-error.title`)} | ${i18n.t(`web.title`)}` }
                defaultTitle={ `${i18n.t(`backend-error.title`)} | ${i18n.t(`web.title`)}` } />

        <header className="navbar navbar-dark navbar-expand fixed-top">
            <a className="navbar-brand" href="/">{ i18n.t(`backend-error.title`) }</a>
        </header>

        <div className="container-fluid">
            <div className="row flex-xl-nowrap">
                <main className="col-12 text-center mt-2" role="main">
                    <h1 className="display-4">{ i18n.t('backend-error.title_h') }</h1>
                    <p className="lead">{ i18n.t('backend-error.subtitle') }</p>
                    <div className="table-responsive mt-4">
                        <p className="mb-0">
                            { i18n.t('backend-error.error_pre') }
                        </p>
                        <pre>{ errorToString(error) }{ '\n\n\n' }</pre>
                    </div>
                </main>
            </div>
        </div>

    </div>
);