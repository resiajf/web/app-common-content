import React from 'react';
import { withNamespaces, WithNamespaces } from 'react-i18next';
import { Link, RouteComponentProps, withRouter } from 'react-router-dom';

import { Button } from 'src/common/components/bootstrap/buttons';
import { Session } from 'src/common/lib/session';
import { User } from 'src/common/redux/user/state';
import { moduleName } from 'src/routes';

import 'hamburgers/dist/hamburgers.min.css';
import { BadgeLink } from 'src/common/components/bootstrap/badge';

interface HeaderProps {
    user: User;
    onSidebarToggle?: (e: React.MouseEvent<HTMLButtonElement>) => void;
    offCanvas?: boolean;
}

type HeaderPropsType = HeaderProps & RouteComponentProps<any> & WithNamespaces;

/**
 * The common header (blue top bar). Shows the display name of the user, the number of notifications unread and a button
 * to close session. If the browser has a phone-like width, it will show a button to show/hide the sidebar.
 */
class Header extends React.PureComponent<HeaderPropsType> {

    private static closeSession(e: React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault();
        Session.deleteSession();
    }

    public render() {
        const { user, onSidebarToggle, offCanvas, t } = this.props;
        const { numNotifications, rol } = user;
        return (
            <header className="navbar navbar-dark navbar-expand fixed-top">
                <button className={ `hamburger hamburger--squeeze d-block d-md-none ${offCanvas ? 'is-active' : ''}` }
                        type="button"
                        onClick={ onSidebarToggle }
                        aria-controls="app-sidebar"
                        aria-expanded="false"
                        aria-label="Toggle navigation">
                    <div className="hamburger-box">
                        <div className="hamburger-inner" />
                    </div>
                </button>
                <Link className="navbar-brand" to="/">{ t(`modules.${moduleName}.title`) }</Link>
                <div className="col" />
                <div className="d-flex align-items-center">
                    <span className="d-none d-sm-block">
                        { rol !== -1 ? user.displayName : t('session.anonymous') }
                    </span>
                    { rol !== -1 &&
                    <BadgeLink href="/notifications" className="ml-0 ml-sm-1" type={ !numNotifications ? 'secondary' : 'success' }>
                        { numNotifications || 0 }
                    </BadgeLink> }
                    <Button type="warning" className="ml-1 ml-sm-2 ml-md-3" outline={ true } size="sm" onClick={ Header.closeSession }>
                        { t('session.close') }
                    </Button>
                </div>
            </header>
        );
    }

}

export default withRouter(withNamespaces()(Header));
