import React from 'react';
import { Helmet } from 'react-helmet';
import { WithNamespaces, withNamespaces } from 'react-i18next';
import { Route, RouteComponentProps, Switch } from 'react-router';
import { toast, ToastContainer } from 'react-toastify';

import asyncComponent from 'src/common/components/async-component';
import Header from 'src/common/components/header';
import Sidebar from 'src/common/components/sidebar';
import { AppDispatchToProps, AppStateToProps } from 'src/common/containers/app/interfaces';
import { SomeErrorsHandler } from 'src/common/containers/some-errors-handler';
import { Notification, Notifications } from 'src/common/lib/api';
import { NewNotificationsChannel } from 'src/common/lib/api/notifications/new-notifications-channel';
import { Session } from 'src/common/lib/session';
import { checkRoutes } from 'src/common/lib/utils';
import { moduleName, routes } from 'src/routes';

import 'react-toastify/dist/ReactToastify.css';
import './app.css';

const PageNotFound = asyncComponent(() => import('src/common/components/page-not-found'));

interface AppState {
    offCanvas: boolean;
}

/**
 * Main entrypoint of the modules. This sets the header (the blue one) and the navigation panel (in the left). Also
 * prepares `Helmet` to allow you to use customized titles inside your module. Moreover, it checks for the user's
 * permissions and shows only the pages (and modules) that has allowed to access. Lastly, prepares a listener for
 * two errors (HTTP 5XX errors and 401 Unauthorized error).
 * @see SomeErrorsHandler
 */
class App extends React.Component<AppStateToProps & AppDispatchToProps & WithNamespaces & RouteComponentProps<{}>, AppState> {

    private notificationsChannel: NewNotificationsChannel;

    public constructor(props: any) {
        super(props);
        this.state = {
            offCanvas: false,
        };

        this.toggleOffCanvas = this.toggleOffCanvas.bind(this);

        if(process.env.NODE_ENV !== 'PRODUCTION') {
            //Checks that the routes are valid (only in development)
            checkRoutes(routes);
        }
    }

    public componentDidMount() {
        if(Session.sessionKey) { //Guests won't receive notifications ;)
            this.notificationsChannel = Notifications.newNotificationsChannel();
            this.notificationsChannel.on('notification', (notification: Notification) => {
                this.props.newNotification(notification);
                toast(<p>
                    {notification.titulo}
                    {notification.cuerpo && <small>
                        <br/>{notification.cuerpo.length < 25 ? notification.cuerpo : notification.cuerpo.substr(0, 25) + '…'}
                    </small>}
                </p>, { autoClose: 6000 });
            }).on('close', () => {
                setTimeout(() => this.componentDidMount(), 5000); //?
            }).on('error', (e) => {
                console.error(e);
            });
        }
    }

    public componentWillUnmount() {
        if(this.notificationsChannel) {
            this.notificationsChannel.removeAllListeners('close').close();
        }
    }

    public render() {
        const { user } = this.props;
        const canSeeThisModule = user.modules.indexOf(moduleName) !== -1;
        const mRoutes = canSeeThisModule ? routes.filter(route => user.pages.indexOf(route.pagePermissionKey) !== -1) : [];

        return (
            <div className="app">

                <Helmet titleTemplate={ `%s - ${this.props.t(`modules.${moduleName}.title`)} | ${this.props.t(`web.title`)}` }
                        defaultTitle={ `${this.props.t(`modules.${moduleName}.title`)} | ${this.props.t(`web.title`)}` } />
                <SomeErrorsHandler />

                <Header user={ user } onSidebarToggle={ this.toggleOffCanvas } offCanvas={ this.state.offCanvas } />

                <div className="container-fluid">
                    <div className="row flex-xl-nowrap">
                        <Sidebar routes={ mRoutes } modules={ user.modules } offCanvas={ this.state.offCanvas } />

                        <ToastContainer />

                        <main className="col-12 col-md-9 col-xl-10 py-md-3 pl-md-3" role="main">
                            <Switch>
                                { mRoutes.map((route, pos) => (
                                    <Route key={ pos }
                                           path={ route.route }
                                           component={ route.component }
                                           {...route.extra} />
                                )) }

                                <Route component={ PageNotFound } />
                            </Switch>
                        </main>
                    </div>
                </div>

            </div>
        );
    }

    private toggleOffCanvas(e: any) {
        e.preventDefault();
        this.setState({ offCanvas: !this.state.offCanvas });
    }
}

export default withNamespaces()(App);
