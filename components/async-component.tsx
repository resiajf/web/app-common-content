import React, { Component } from 'react';
import LoadSpinner from './load-spinner';

/**
 * Allows you to decorate a component for code splitting. Your component will be in a different `.js` file (if you
 * always use the component with the decorator) and it will load when needed. When loading it will show a
 * {@link LoadSpinner} or any component you define.
 * @param importComponent Function that will be called when the component is mounted, and it will load your component
 * if it is not already loaded.
 * @param Loading Component that will be shown while your component is loading. By default uses {@link LoadSpinner}.
 * @see https://serverless-stack.com/chapters/code-splitting-in-create-react-app.html
 * @example
 * const Component = asyncComponent(() => import('./path/to/my/component'));
 * ...
 * <Component one="prop" two={ `${props}` } /> //Use it as usual, it will load automatically and show when loaded
 */
export default function asyncComponent(importComponent: () => any, Loading: React.ComponentType<{}> = LoadSpinner) {
    class AsyncComponent extends Component<any, { component: React.ComponentType | any | null }> {
        constructor(props: any) {
            super(props);

            this.state = {
                component: null
            };
        }

        public async componentDidMount() {
            const { default: component, Component: comp } = await importComponent();

            this.setState({
                component: comp || component
            });
        }

        public render() {
            const C = this.state.component;
            return C ? <C {...this.props} /> :
                <div className="d-flex justify-content-center mt-4"><Loading /></div>;
        }
    }

    return AsyncComponent;
}
