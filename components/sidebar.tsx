import pathToRegexp from 'path-to-regexp';
import React from 'react';
import { WithNamespaces, withNamespaces } from 'react-i18next';
import { Link, RouteComponentProps, withRouter } from 'react-router-dom';

import { Route } from 'src/common/lib/utils';
import { moduleName } from 'src/routes';

interface SidebarProps {
    modules: string[];
    routes: Array<Route<any, any>>;
    offCanvas: boolean;
}

type SidebarPropsType = SidebarProps & RouteComponentProps<any> & WithNamespaces;

/**
 * The sidebar component. Shows a list of modules, and for the current module, a list of pages. It is hidden if the
 * width of the browser is too small. But it will show with a button in the header.
 * The order of the modules and pages are sorted lexicographically with the translated strings.
 * @todo If a page has pagination (like `/` goes to `/1`), it won't show as selected.
 */
class Sidebar extends React.PureComponent<SidebarPropsType> {

    public constructor(props: SidebarPropsType) {
        super(props);

        this.navLinkPressed = this.navLinkPressed.bind(this);
    }

    public render() {
        const { modules, offCanvas, t } = this.props;
        return (
            <div className="col-12 col-md-3 col-xl-2 app-sidebar">
                <nav className={ `collapse sidebar-collapse offcanvas-collapse ${offCanvas ? 'open' : ''}` }
                     id="app-sidebar">
                    {
                        modules
                            .sort((a, b) => t(`modules.${a}.title`).localeCompare(t(`modules.${b}.title`)))
                            .map((module, i) => (
                            <div className={`sidebar-link ${module === moduleName ? 'active' : ''}`} key={ i }>
                                <a href={ `/${module}/` }>{ t(`modules.${module}.title`) }</a>
                                { module === moduleName && this.generateRouteLinks() }
                            </div>
                        ))
                    }
                </nav>
            </div>
        );
    }

    private isLinkActive(route: Route) {
        if(route.extra && route.extra.exact) {
            return this.props.location.pathname === route.route ? 'active' : '';
        } else {
            const re = pathToRegexp(route.route);
            return this.props.location.pathname.match(re) ? 'active' : '';
        }
    }

    private generateRouteLinks() {
        return <ul className="nav app-sidebar-nav">{
            this.props.routes.filter(route => !route.extra.hide).map((route, pos) => (
                <li className={ this.isLinkActive(route) } key={ pos }>
                    <Link to={ route.route }
                          className="nav-link"
                          onClick={ this.navLinkPressed }>
                        { this.props.t(`modules.${moduleName}.pages.${route.routeKey}`, { defaultValue: route.routeKey }) }
                    </Link>
                </li>
            ))
        }</ul>;
    }

    private navLinkPressed() {
        this.setState({ offCanvas: false });
    }

}

export default withRouter(withNamespaces()(Sidebar));
