import React from 'react';

/**
 * Base properties for {@link Badge} and {@link BadgeLink}.
 * @see http://getbootstrap.com/docs/4.1/components/badge/ Bootstrap's badge documentation
 */
interface BadgePropsBase {
    type: 'primary' | 'secondary' | 'success' | 'danger' | 'warning' | 'info' | 'light' | 'dark';
    pill?: boolean;
    className?: string;
}

type BadgeProps = BadgePropsBase & React.HTMLAttributes<HTMLSpanElement>;
type BadgeLinkProps = BadgePropsBase & React.AnchorHTMLAttributes<HTMLAnchorElement>;

const generateClasses = ({ type, pill, className }: BadgePropsBase): string => {
    let classes = [ 'badge', `badge-${type}` ];
    if(pill) {
        classes = classes.concat('badge-pill');
    }
    if(className) {
        classes = classes.concat(className);
    }
    return classes.join(' ');
};

/**
 * A badge from Bootstrap's badge component. You can use any HTML attribute that applies to a `<span/>` element.
 * @see BadgePropsBase
 * @see http://getbootstrap.com/docs/4.1/components/badge/ Bootstrap's badge documentation
 */
export const Badge = ({ type, pill, className, children, ...props }: BadgeProps) => (
    <span className={ generateClasses({ type, pill, className }) } {...props}>
        { children }
    </span>
);

/**
 * A badge link from Bootstrap's badge component. You can use any HTML attribute that applies to a `<a/>` element.
 * @see BadgePropsBase
 * @see http://getbootstrap.com/docs/4.1/components/badge/ Bootstrap's badge documentation
 * @see http://getbootstrap.com/docs/4.1/components/badge/#links Bootstrap's badge documentation (link section)
 */
export const BadgeLink = ({ type, pill, className, href, children, ...props }: BadgeLinkProps) => (
    <a className={ generateClasses({ type, pill, className }) } href={ href || '#' } {...props}>
        { children }
    </a>
);