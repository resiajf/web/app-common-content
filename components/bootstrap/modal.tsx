import React from 'react';

const $ = require('jquery');

interface ModalProps {
    verticalCentered?: boolean;
    animationClass?: 'fade' | string | false;
    size?: 'sm' | 'lg';
    show: boolean;
    onShow?: () => void;
    onShown?: () => void;
    onClose?: () => void;
    onClosed?: () => void;
}

/**
 * Implements a Bootstrap Modal component. The element prepares the envelope of a modal, but you should fill the modal
 * with {@link ModalHeader}, {@link ModalBody} and (optional) {@link ModalFooter}. To open the modal, send a `true` to
 * the `show` property, and to close, a `false` to the same. You can customize some properties of the modal as seen
 * in the documentation. It also contains callbacks for the events of a modal.
 * @see ModalProps
 * @see http://getbootstrap.com/docs/4.1/components/modal/ Bootstrap's Modal documentation
 * @example
 * <Modal show={ showModal } onClosed={ this.onModalClose }>
 *     <ModalHeader title="Modal title" onClose={ this.onModalClose } />
 *     <ModalBody>
 *         <p>Modal body text goes here.</p>
 *     </ModalBody>
 *     <ModalFooter>
 *         <Button type="secondary" onClick={ this.onModalClose }>Close</Button>
 *         <Button type="primary">Save changes</Button>
 *     </ModalFooter>
 * </Modal>
 *
 * onModalClose() { this.setState({ showModal: false }); }
 */
export class Modal extends React.Component<ModalProps & React.HTMLAttributes<HTMLDivElement>> {

    private ref: React.Ref<HTMLDivElement> = React.createRef();

    public componentDidMount() {
        $(this.modalDiv).modal({
            show: this.props.show,
        });

        if(this.props.onClosed) {
            $(this.modalDiv)
                .on('show.bs.modal', () => this.props.onShow && this.props.onShow())
                .on('shown.bs.modal', () => this.props.onShown && this.props.onShown())
                .on('hide.bs.modal', () => this.props.onClose && this.props.onClose())
                .on('hidden.bs.modal', () => this.props.onClosed && this.props.onClosed());
        }
    }

    public componentDidUpdate(prevProps: Readonly<ModalProps>) {
        if(prevProps.show !== this.props.show) {
            if(this.props.show) {
                $(this.modalDiv).modal('show');
            } else {
                $(this.modalDiv).modal('hide');
            }
        }
    }

    public componentWillUnmount() {
        $(this.modalDiv).modal('dispose');
    }

    public render() {
        const { animationClass, verticalCentered, size, children, className, show, onShow, onClose, onShown, onClosed, ...props } = this.props;
        let modalClasses = [ 'modal' ];
        let modalDialogClasses = [ 'modal-dialog' ];
        if(animationClass !== false) {
            modalClasses = modalClasses.concat(animationClass || 'fade');
        }
        if(verticalCentered) {
            modalDialogClasses = modalDialogClasses.concat('modal-dialog-centered');
        }
        if(size === 'lg') {
            modalDialogClasses = modalDialogClasses.concat('modal-lg');
        } else if(size === 'sm') {
            modalDialogClasses = modalDialogClasses.concat('modal-sm');
        }
        if(className) {
            modalClasses = modalClasses.concat(className);
        }
        return (
            <div className={ modalClasses.join(' ') } tabIndex={ -1 } role="dialog" ref={ this.ref } { ...props }>
                <div className={ modalDialogClasses.join(' ') } role="document">
                    <div className="modal-content">
                        { children }
                    </div>
                </div>
            </div>
        );
    }

    private get modalDiv() {
        return (this.ref!.valueOf() as React.RefObject<HTMLDivElement>).current!;
    }

}

/**
 * The element for the modal header. If you set a `title`, it will add the element for you. If you set the `onClose`
 * listener, it will add the close button for you. You can add any other content if you want.
 */
export const ModalHeader = ({ title, children, onClose, className, ...props }: { title?: string, onClose?: (e?: any) => void } & React.HTMLAttributes<HTMLDivElement>) => (
    <div className={ `modal-header ${className ? className : ''}` } { ...props }>
        { title && <h5 className="modal-title">{ title }</h5> }
        { children }
        { onClose && <button type="button" className="close" onClick={ onClose } aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button> }
    </div>
);

/**
 * The element for the body of a modal. Put your content inside this element to build the body of the modal.
 */
export const ModalBody = ({ children, className, ...props }: React.HTMLAttributes<HTMLDivElement>) => (
    <div className={ `modal-body ${className ? className : ''}` } { ...props }>
        { children }
    </div>
);

/**
 * The element for the footer of a modal. You can put {@link Button}s on it :)
 */
export const ModalFooter = ({ children, className, ...props }: React.HTMLAttributes<HTMLDivElement>) => (
    <div className={ `modal-footer ${className ? className : ''}` } { ...props }>
        { children }
    </div>
);
