import React from 'react';

const $ = require('jquery');

interface CollapseProps {
    show: boolean;
    onShow?: () => void;
    onShown?: () => void;
    onHide?: () => void;
    onHidden?: () => void;
    className?: string;
}

/**
 * Implements the Collapse component from Bootstrap using JavaScript. To show the collapse, send a `true` in the `show`
 * property. To close, simply send a `false` to the same property. Has callbacks for the four events of a collapse.
 * @see CollapseProps
 * @see http://getbootstrap.com/docs/4.1/components/collapse/ Bootstrap's Collapse documentation
 * @example
 * <Collapse show={ showCollapse }>
 *     <p>Your content here <i>:)</i></p>
 * </Collapse>
 */
export class Collapse extends React.Component<CollapseProps & React.HTMLAttributes<HTMLDivElement>> {

    private ref: React.RefObject<HTMLDivElement> = React.createRef();

    public componentDidMount() {
        $(this.ref.current!)
            .collapse({ toggle: this.props.show })
            .on('show.bs.collapse', () => {
                if(this.props.onShow) {
                    this.props.onShow();
                }
            })
            .on('shown.bs.collapse', () => {
                if(this.props.onShown) {
                    this.props.onShown();
                }
            })
            .on('hide.bs.collapse', () => {
                if(this.props.onHide) {
                    this.props.onHide();
                }
            })
            .on('hidden.bs.collapse', () => {
                if(this.props.onHidden) {
                    this.props.onHidden();
                }
            });
    }

    public componentWillUnmount() {
        $(this.ref.current!).collapse('dispose');
    }

    public componentDidUpdate(prevProps: Readonly<CollapseProps>) {
        if(prevProps.show !== this.props.show) {
            if(this.props.show) {
                $(this.ref.current!).collapse('show');
            } else {
                $(this.ref.current!).collapse('hide');
            }
        }
    }

    public render() {
        const { className, show, children, onShow, onShown, onHide, onHidden, ...props } = this.props;

        return (
            <div className={ `collapse ${className || ''}` } ref={ this.ref } aria-expanded={ show } { ...props }>
                { children }
            </div>
        );
    }

}