import React from 'react';

type BootstrapButtonType = 'primary' | 'secondary' | 'success' | 'danger' | 'warning' | 'info' | 'light' | 'dark' | 'link';

/**
 * Properties for a Button.
 * @see http://getbootstrap.com/docs/4.1/components/buttons/ Bootstrap's documentation for Button
 */
interface BootstrapButtonPropsBase {
    type: BootstrapButtonType;
    outline?: boolean;
    size?: 'lg' | 'sm';
    block?: boolean;
    active?: boolean;
    className?: string;
}

type BootstrapButtonProps = BootstrapButtonPropsBase & React.ButtonHTMLAttributes<HTMLButtonElement>;
type BootstrapButtonLinkProps = BootstrapButtonPropsBase & React.AnchorHTMLAttributes<HTMLAnchorElement>;
type BootstrapButtonInputProps = BootstrapButtonPropsBase & React.InputHTMLAttributes<HTMLInputElement> & { inputType: 'button' | 'submit' | 'reset' };

/**
 * Applies classes for buttons depending on the properties.
 * @returns The resulting classes for the properties to make a button as desired.
 */
function getClasses({ type, outline, size, block, active, className, ...props }: BootstrapButtonPropsBase): string {
    let classes = [ 'btn' ];
    if(outline) {
        classes = classes.concat(`btn-outline-${type}`);
    } else {
        classes = classes.concat(`btn-${type}`);
    }

    if(size === 'lg') {
        classes = classes.concat('btn-lg');
    } else if(size === 'sm') {
        classes = classes.concat('btn-sm');
    }

    if(block) {
        classes = classes.concat('btn-block');
    }

    if(active) {
        classes = classes.concat('active');
        props['aria-pressed'] = 'true';
    }

    if(className) {
        classes = classes.concat(className);
    }

    return classes.join(' ');
}

/**
 * A Button as seen in Bootstrap. Accepts all `<button/>` attributes plus {@link BootstrapButtonPropsBase} for making
 * a button with the right design. <b>Note</b> button plugin does not apply to this, only styling.
 * @see BootstrapButtonPropsBase
 * @see http://getbootstrap.com/docs/4.1/components/buttons/ Bootstrap's Button documentation
 */
const BootstrapButton = ({ type, outline, size, block, active, children, className, ...props }: BootstrapButtonProps) => {
    return (
        <button className={ getClasses({ type, outline, size, block, active, className }) } { ...props }>
            { children }
        </button>
    );
};

/**
 * A Button as seen in Bootstrap using links. Accepts all `<a/>` attributes (except `role`) plus
 * {@link BootstrapButtonPropsBase} for making a button with the right design. <b>Note</b> button plugin does not apply
 * to this, only styling.
 * @see BootstrapButtonPropsBase
 * @see http://getbootstrap.com/docs/4.1/components/buttons/ Bootstrap's Button documentation
 */
const BootstrapButtonLink = ({ type, outline, size, block, active, children, className, href, role, ...props }: BootstrapButtonLinkProps) => {
    return (
        <a className={ getClasses({ type, outline, size, block, active, className }) } href={ href || '#' } role="button" { ...props }>
            { children }
        </a>
    );
};

/**
 * A Button as seen in Bootstrap using input. Accepts all `<input/>` attributes (`type` is `inputType`) plus
 * {@link BootstrapButtonPropsBase} for making a button with the right design. <b>Note</b> button plugin does not apply
 * to this, only styling.
 * @see BootstrapButtonPropsBase
 * @see http://getbootstrap.com/docs/4.1/components/buttons/ Bootstrap's Button documentation
 */
const BootstrapButtonInput = ({ type, outline, size, block, active, children, className, inputType, ...props }: BootstrapButtonInputProps) => {
    return (
        <input className={ getClasses({ type, outline, size, block, active, className }) } type={ inputType } { ...props } />
    );
};


export const Button = BootstrapButton;
export const Link = BootstrapButtonLink;
export const Input = BootstrapButtonInput;
