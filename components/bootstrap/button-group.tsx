import React from 'react';

/**
 * Allows you to join some `Button`s as seen in Bootstrap. You can use any of the `<div/>` attributes except `role`.
 * Adds attributes `size` (for custom button sizes) and `vertical` (to group buttons in vertical).
 * @see http://getbootstrap.com/docs/4.1/components/button-group/ Bootstrap's Button Group documentation
 */
export const ButtonGroup = ({ size, vertical, className, role, children, ...props }: React.HTMLAttributes<HTMLDivElement> & { size?: 'lg' | 'sm', vertical?: boolean }) => {
    let classes = [ 'btn-group' ];
    if(className) {
        classes = classes.concat(className);
    }
    if(size === 'lg') {
        classes = classes.concat('btn-group-lg');
    } else if(size === 'sm') {
        classes = classes.concat('btn-group-sm');
    }
    if(vertical) {
        classes = classes.concat('btn-group-vertical');
    }
    return (
        <div className={ classes.join(' ') } role="group" {...props}>
            { children }
        </div>
    );
};

/**
 * Allows you to group some {@link ButtonGroup}s to make a toolbar. You can use any of the `<div/>` attributes except `role`.
 * @see http://getbootstrap.com/docs/4.1/components/button-group/#button-toolbar Bootstrap's Button Toolbar documentation
 */
export const ButtonToolbar = ({ className, role, children, ...props }: React.HTMLAttributes<HTMLDivElement>) => {
    let classes = [ 'btn-toolbar' ];
    if(className) {
        classes = classes.concat(className);
    }
    return (
        <div className={ classes.join(' ') } role="toolbar" {...props}>
            { children }
        </div>
    );
};