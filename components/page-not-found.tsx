import React from 'react';
import { Trans, WithNamespaces, withNamespaces } from 'react-i18next';
import { RouteComponentProps, withRouter } from 'react-router';

/**
 * The common page not found page (the error 404 one yes). Has nothing special on it.
 */
class PageNotFound extends React.PureComponent<WithNamespaces & RouteComponentProps<any>> {

    constructor(props: any) {
        super(props);

        this.goBack = this.goBack.bind(this);
    }

    public render() {
        const { t } = this.props;

        return (
            <div className="text-center">
                <h1 className="display-4">{ t('not-found.title') }</h1>
                <p className="lead">{ this.randomSubtitle() }</p>
                <p className="mt-4">
                    <Trans i18nKey="not-found.options">
                        You've got some options, <a href="#" onClick={ this.goBack }>go back</a> or go to the <a href="/">main page</a>.
                    </Trans>
                </p>
            </div>
        );
    }

    private randomSubtitle() {
        const a: string[] = this.props.t('not-found.subtitles', { returnObjects: true }) || [ '¿' ];
        const b = Math.trunc(a.length * Math.random());
        return a[b];
    }

    private goBack(e: React.MouseEvent<HTMLAnchorElement>) {
        e.preventDefault();
        if(this.props.history.length > 0) {
            this.props.history.goBack();
        } else {
            window.history.back();
        }
    }

}

// @ts-ignore
export default withRouter(withNamespaces()(PageNotFound));