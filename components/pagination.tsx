import React from 'react';
import { WithNamespaces, withNamespaces } from 'react-i18next';

function range(start: number, end: number): number[] {
    const a = [];
    for(let i = start; i < end; i++) {
        a.push(i);
    }
    return a;
}

const PaginationItem = ({ page, i, onChange }: { page: number, i: number, onChange: (page: number) => void }) => (
    <li className={`page-item ${i === page ? 'active' : ''}`}>
        <a className="page-link" tabIndex={ i } onClick={ onChange.bind(null, i) }>{i + 1}</a>
    </li>
);

/**
 * Properties for the {@link Pagination} component.
 */
interface PaginationProps extends WithNamespaces {
    /**
     * The current page (starts from 0).
     */
    page: number;

    /**
     * Total of pages available (0-indexed).
     */
    pages: number;

    /**
     * Function that will be called when user decides to change the page. It will tell you which page to change.
     */
    onChange: (page: number) => void;

    /**
     * Add custom classes if you want.
     */
    className?: string;
}

/**
 * Shows a beautiful pagination based in bootstrap. It will handle all the logic for passing pages and showing the pages
 * shortcuts. You only have to implement a generic `go to page` function (`onChange`). The pages must be 0-indexed
 * (start from 0 and go to `pages - 1`). Uses Bootstrap pagination design.
 * @see PaginationProps
 * @see http://getbootstrap.com/docs/4.1/components/pagination/ Bootstrap's pagination documentation
 */
const Pagination = ({ page, pages, onChange, className, t }: PaginationProps) => {
    let prePage = page - 2;
    let postPage = page + 3;

    if(prePage < 1) {
        const diff = -prePage;
        prePage = 1;
        postPage += diff + 1;
    }

    if(postPage > pages - 1) {
        const diff = postPage - pages + 1;
        postPage = pages - 1;
        prePage = Math.max(1, prePage - diff);
    }

    if(prePage > 1) {
        prePage++;
    }
    if(postPage < pages - 1) {
        postPage--;
    }

    const prev = () => onChange(page - 1);
    const next = () => onChange(page + 1);

    return ( pages > 1 ?
        <nav aria-label="Page navigation example" className={className}>
            <ul className="pagination pagination-sm justify-content-center">
                <li className={`page-item ${page === 0 ? 'disabled' : ''}`}>
                    <a className="page-link" tabIndex={ -1 } onClick={ prev }>
                        <span aria-hidden="true">&laquo;</span>
                        <span className="sr-only">{ t('pagination.prev') }</span>
                    </a>
                </li>

                <PaginationItem page={page} i={0} onChange={onChange} />
                { prePage > 1 && <li className="page-item disabled"><a className="page-link">…</a></li> }
                {range(prePage, postPage).map(i => <PaginationItem key={i} page={page} i={i} onChange={onChange} />)}
                { postPage < pages - 1 && <li className="page-item disabled"><a className="page-link">…</a></li> }
                <PaginationItem page={page} i={pages-1} onChange={onChange} />

                <li className={`page-item ${page === pages - 1 ? 'disabled' : ''}`}>
                    <a className="page-link" tabIndex={ +1 } onClick={ next }>
                        <span aria-hidden="true">&raquo;</span>
                        <span className="sr-only">{ t('pagination.next') }</span>
                    </a>
                </li>
            </ul>
        </nav> : null
    );
};

/**
 * @see Pagination
 */
export default withNamespaces()(Pagination);