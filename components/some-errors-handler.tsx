import React from 'react';
import { WithNamespaces, withNamespaces } from 'react-i18next';
import { RouteComponentProps } from 'react-router';
import { toast } from 'react-toastify';

import { SomeErrorsHandlerStateToProps } from 'src/common/containers/some-errors-handler';
import { Session } from 'src/common/lib/session';
import { ErrorsState } from 'src/common/redux/errors/state';

type SomeErrorsHandlerProps = WithNamespaces & RouteComponentProps<{}> & SomeErrorsHandlerStateToProps & { clearError: (what: keyof ErrorsState) => void; };

const ErrorText = ({ key, error, t }: any) => (
    <p>
        { t(key) }
        <br />
        <small>{ t(error.translatedMessageKey) }</small>
    </p>
);

/**
 * Handles the HTTP 5XX error by showing a toast (with `react-toastify`). For the Unauthorized error (401), it will
 * show a toast too, but when it closes, it will redirect to the home page (`/`).
 */
class SomeErrorsHandlerClass extends React.PureComponent<SomeErrorsHandlerProps> {

    public componentDidUpdate(prevProps: Readonly<SomeErrorsHandlerProps>) {
        if(!prevProps.serverUnavailable && this.props.serverUnavailable) {
            //It will show up something, but the error will be cleared (if not done already) when the notification is closed
            toast.error(
                <ErrorText key="error-handler.server-error" error={ this.props.serverUnavailable } t={ this.props.t } />,
                { onClose: () => this.props.serverUnavailable && this.props.clearError('serverUnavailable') }
            );
        }

        if(!prevProps.unauthorized && this.props.unauthorized) {
            //When the notification is closed, it will redirect to the home page to login
            toast.error(
                <ErrorText key="error-handler.unauthorized" error={ this.props.unauthorized } t={ this.props.t } />, {
                onClose: () => Session.logIn(),
            });
        }

        if(!prevProps.forbidden && this.props.forbidden) {
            //Wait some time to see if the error is not being handled
            setTimeout(() => {
                if(this.props.forbidden) {
                    console.error('Unhandled error', this.props.forbidden);
                    toast.error(<ErrorText key="error-handler.forbidden" error={ this.props.forbidden } t={ this.props.t } />);
                    setTimeout(() => this.props.clearError('forbidden'));
                }
            }, 100);
        }

        if(!prevProps.invalid && this.props.invalid) {
            //Wait some time to see if the error is not being handled
            setTimeout(() => {
                if(this.props.invalid) {
                    console.error('Unhandled error', this.props.invalid);
                    toast.error(<ErrorText key="error-handler.invalid" error={ this.props.invalid } t={ this.props.t } />);
                    setTimeout(() => this.props.clearError('invalid'));
                }
            }, 100);
        }

        if(!prevProps.notFound && this.props.notFound) {
            //Wait some time to see if the error is not being handled
            setTimeout(() => {
                if(this.props.notFound) {
                    console.error('Unhandled error', this.props.notFound);
                    toast.error(<ErrorText key="error-handler.not-found" error={ this.props.notFound } t={ this.props.t } />);
                    setTimeout(() => this.props.clearError('notFound'));
                }
            }, 100);
        }
    }

    public render() {
        return null;
    }

}

export const SomeErrorsHandler = withNamespaces()(SomeErrorsHandlerClass);
