import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import { SomeErrorsHandler as Component } from 'src/common/components/some-errors-handler';
import { clearError } from 'src/common/redux/errors/actions';
import { ErrorsState } from 'src/common/redux/errors/state';
import { State } from 'src/redux';

export type SomeErrorsHandlerStateToProps = ErrorsState;

const mapStateToProps = ({ errors }: State): SomeErrorsHandlerStateToProps => ({
    ...errors,
});

const mapDispatchToProps = (dispatch: any) => ({
    clearError: (what: keyof ErrorsState) => dispatch(clearError(what)),
});

export const SomeErrorsHandler = withRouter(connect(mapStateToProps, mapDispatchToProps)(Component));
