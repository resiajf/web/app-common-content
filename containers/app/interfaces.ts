import { Notification } from 'src/common/lib/api';
import { User } from 'src/common/redux/user/state';

export interface AppStateToProps {
    user: User;
}

export interface AppDispatchToProps {
    newNotification: (notification: Notification) => void;
}
