import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Dispatch } from 'redux';

import App from 'src/common/components/app/app';
import { AppStateToProps } from 'src/common/containers/app/interfaces';
import { Notification } from 'src/common/lib/api';
import { State } from 'src/common/redux/';

const mapStateToProps = (state: State): AppStateToProps => ({
    user: state.user
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
    newNotification: (notification: Notification) => dispatch({ type: 'NEW_NOTIFICATION', notification }),
});

// @ts-ignore
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
