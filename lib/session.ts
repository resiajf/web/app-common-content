import { User } from '../redux/user/state';
import { SelfUser, Users } from './api';
import { UnauthorizedError } from './api/errors';
import { localStorage, sessionStorage } from './navigator-storage';

/**
 * Functions for managing the session
 */
export class Session {

    public static sessionKey: string | null = null;

    /**
     * Checks if there's a valid session stored in the localStorage, sessionStorage or in the query parameter `token`.
     * If it is valid, then will return the user itself. If not, it will throw an error. Also throws errors for errors
     * trying to get that valid session.
     * @see UnauthorizedError
     */
    public static async checkSession(allowAnonymous?: boolean): Promise<SelfUser> {
        let session: string;
        let user: SelfUser;
        const query: { [index: string]: string } = window.location.search
            .substr(1)
            .split('&')
            .map(e => e.split('='))
            .reduce((x, e) => ({
                ...x,
                [e[0]]: e[1]
            }), {});

        if(query.token) {
            //We received a token from the backend (maybe)...
            const type = localStorage.getItem('session-persistent', false)!;
            const redirect = localStorage.getItem<string>('session-origin-url');
            this.sessionKey = session = query.token;

            //Let's check it
            try {
                user = await Users.usuario();

                //Its valid, let's continue
                if(type) {
                    localStorage.setItem('session', query.token);
                } else {
                    sessionStorage.setItem('session', query.token);
                }
                if(redirect) {
                    //Remove stored URL because we don't need it anymore
                    localStorage.removeItem('session-origin-url');
                    window.location.assign(redirect);
                }

                return user;
            } catch(e) {
                //Is invalid :(
                this.sessionKey = null;
                //Check other options...
            }
        }

        if(localStorage.getItem<string>('session')) {
            //Session is in localStorage
            session = localStorage.getItem<string>('session')!;
        } else if(sessionStorage.getItem<string>('session')) {
            //Session is in sessionStorage
            session = sessionStorage.getItem<string>('session')!;
        } else  {
            localStorage.setItem('session-origin-url', window.location.toString());
            if(window.location.pathname !== '/' && !allowAnonymous) {
                window.location.assign('/');
                session = 'useless';
            } else {
                //If we are in the login page, don't redirect :)
                //If the module allows to be anonymous, don't redirect :)
                //But maybe this trick is not quite good :(
                throw null;
            }
        }

        Session.sessionKey = session;
        try {
            return await Users.usuario();
        } catch(e) {
            if(e instanceof UnauthorizedError) {
                //The session is invalid :(
                //Relogin. This should return to this page when the login process ends.
                Session.logIn();
            }
            //This is to silence error in TS for the `UnauthorizedError`
            throw e;
        }
    }

    /**
     * After renewing the token, modifies all the stored tokens to this new one. Supposes the token is valid.
     * @param token The new access token.
     */
    public static renewToken(token: string) {
        if(localStorage.getItem<string>('session')) {
            localStorage.setItem('session', token);
        } else if(sessionStorage.getItem<string>('session')) {
            sessionStorage.setItem('session', token);
        }
        Session.sessionKey = token;
    }

    /**
     * Stores the token in the local or session storage. Supposes the token is valid.
     * @param token The new access token.
     * @param persistent If it should stored in localStorage (if true) or in session storage (if false).
     */
    public static storeToken(token: string, persistent?: boolean) {
        if(persistent === undefined) {
            persistent = localStorage.getItem<boolean>('session-persistent', false)!;
        }

        if(persistent) {
            localStorage.setItem('session', token);
        } else {
            sessionStorage.setItem('session', token);
        }
        Session.sessionKey = token;
    }

    /**
     * Coverts a {@link SelfUser} to a {@link User} for usage in the redux states.
     * @param user A {@link SelfUser}
     * @param thisModuleName The name of this module
     */
    public static convertToStatusUser(user: SelfUser, thisModuleName: string): User {
        const modulesSet = new Set<string>();
        const pagesSet = new Set<string>();
        const permissionsPerPage = new Map<string, string[]>();
        const permissionsRelatedToThisModule = user.info_permisos.permisos
            .filter(permission => permission.module_name === thisModuleName);
        const pagesPerModule = new Map<string, Set<string>>();

        user.info_permisos.permisos.forEach(permission => modulesSet.add(permission.module_name));
        permissionsRelatedToThisModule.forEach(permission => pagesSet.add(permission.page_name));
        pagesSet.forEach(pageName => permissionsPerPage.set(pageName, []));
        permissionsRelatedToThisModule.forEach(permission => (
            //Adds a new entry in the array for the page association
            permissionsPerPage.set(permission.page_name, [...permissionsPerPage.get(permission.page_name)!, permission.permission])
        ));
        modulesSet.forEach(moduleName => pagesPerModule.set(moduleName, new Set()));
        user.info_permisos.permisos.forEach(permission => pagesPerModule.get(permission.module_name)!.add(permission.page_name));

        return {
            apartment: user.usuario.apartment || undefined,
            displayName: user.usuario.display_name,
            modules: Array.from(modulesSet.values()),
            niu: user.usuario.niu,
            numNotifications: user.unread_notifications,
            pages: Array.from(pagesSet),
            pagesPerModule: Array.from(pagesPerModule.entries())
              .reduce((obj, [key, value]) => ({ ...obj, [key]: Array.from(value.keys()) }), {}),
            permissionsPerPage: Array.from(permissionsPerPage.entries()).reduce((obj, [key, value]) => ({ ...obj, [key]: value }), {}),
            rol: user.usuario.rol_id_rol,
        };
    }

    public static anonymousUser(moduleName: string): User {
        return this.convertToStatusUser({
            info_permisos: {
                permisos: [
                    { module_name: 'news', page_name: 'home', permission: 'all' },
                    { module_name: 'news', page_name: 'stay_connected', permission: 'all' },
                    { module_name: 'intos', page_name: 'all', permission: 'all' },
                ].map((a, i) => ({ ...a, id_permiso: i, permission_desc: '' })),
                rol: { id_rol: -1, name: 'guest' },
            },
            unread_notifications: 0,
            usuario: {
                apartment: null,
                display_name: 'Guest',
                niu: NaN,
                rol_id_rol: -1,
            },
        }, moduleName);
    }

    /**
     * Starts the login process, storing the current page for returning after the whole process ends (if not `/`).
     */
    public static logIn(): void {
        if(window.location.pathname !== '/') {
            localStorage.setItem('session-origin-url', window.location.toString());
        }
        //window.location.assign('/alberginia/');
        window.location.assign('/');
    }

    /**
     * Deletes the session (closes it).
     */
    public static deleteSession(): void {
        const isAnonymous = sessionStorage.getItem('anonymous-login', false);
        sessionStorage.removeItem('anonymous-login');
        localStorage.removeItem('session');
        sessionStorage.removeItem('session');
        localStorage.removeItem('session-persistent');
        if(!isAnonymous) {
            window.location.assign('/alberginia/logout?ReturnTo=/'); //TODO test it
        } else {
            window.location.assign('/');
        }
    }

}
