import i18n from 'i18next';
import moment from 'moment';
import { reactI18nextModule } from 'react-i18next';

import { moduleName } from 'src/routes';

const Backend = require('i18next-xhr-backend');
const LanguageDetector = require('i18next-browser-languagedetector');

import 'moment/locale/ca'; //Català
import 'moment/locale/en-gb'; //British English (american english is the default)
import 'moment/locale/es'; //Castellano
import 'moment/locale/eu'; //Euskera
import 'moment/locale/gl'; //Galego

/*
 * This script prepares the localization of the module. For that, it will look for `/locales/LANG/{MODULE_NAME|common}.json`
 * files and it will load. First will try with the browser's languages, but if it doesn't have any luck with that, it will
 * default to 'spanish'. Also configures moment (a package for dates) to use the same language.
 *
 * To add localization to your components, use `translate()(YourComponent)` and use the returned component in the React
 * tree. This will add a `t` prop that you can use to translate things. If you need a more customized translations,
 * use Trans react component. See https://react.i18next.com and https://www.i18next.com
 *
 * For moment, see https://momentjs.com
 */

i18n
    .use(Backend)
    .use(LanguageDetector)
    .use(reactI18nextModule)
    .init({
        backend: {
            loadPath: process.env.NODE_ENV !== 'PRODUCTION' ?
                '/locales/{{lng}}/{{ns}}.json' :
                '/locales/{{lng}}/{{ns}}.json'
        },
        debug: process.env.NODE_ENV !== 'PRODUCTION',
        defaultNS: moduleName,
        detection: {
            caches: ['localStorage'],
            lookupLocalStorage: 'app-panel:lang',
        },
        fallbackLng: 'es',
        fallbackNS: 'common',
        interpolation: {
            escapeValue: false,
        },
        ns: ['common', moduleName],
        react: {
            wait: true
        }
    })
    .on('initialized', () => {
        //Use by default spanish, same as default locale in i18next
        moment.locale([ i18n.language, 'es' ]);
    });

export default i18n;