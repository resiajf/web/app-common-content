export abstract class NavigatorStorage {

    public getItem<T>(key: string, defaultValue: T): T;
    public getItem<T>(key: string, defaultValue: null): null;
    public getItem<T>(key: string, defaultValue?: T | null): T | null;
    public getItem<T>(key: string, defaultValue: T | null = null): T | null {
        const value = this.getItemImpl(`app-panel:${key}`);
        if(value === null) {
            return defaultValue;
        }

        return JSON.parse(value);
    }

    public setItem<T>(key: string, value: T): void {
        this.setItemImpl(`app-panel:${key}`, JSON.stringify(value));
    }

    public removeItem(key: string): void {
        this.removeItemImpl(`app-panel:${key}`);
    }

    protected abstract getItemImpl(key: string): string | null;
    protected abstract setItemImpl(key: string, value: string): void;
    protected abstract removeItemImpl(key: string): void;

}