import { NavigatorStorage } from 'src/common/lib/navigator-storage/navigator-storage';

export class SessionStorage extends NavigatorStorage {

    protected removeItemImpl(key: string): void {
        window.sessionStorage.removeItem(key);
    }

    protected getItemImpl(key: string): string | null {
        return window.sessionStorage.getItem(key);
    }

    protected setItemImpl(key: string, value: string): void {
        return window.sessionStorage.setItem(key, value);
    }

}