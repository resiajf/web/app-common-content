import { LocalStorage } from './local-storage';
import { SessionStorage } from './session-storage';

export const localStorage = new LocalStorage();
export const sessionStorage = new SessionStorage();
