import { NavigatorStorage } from 'src/common/lib/navigator-storage/navigator-storage';

export class LocalStorage extends NavigatorStorage {

    protected removeItemImpl(key: string): void {
        window.localStorage.removeItem(key);
    }

    protected getItemImpl(key: string): string | null {
        return window.localStorage.getItem(key);
    }

    protected setItemImpl(key: string, value: string): void {
        return window.localStorage.setItem(key, value);
    }

}