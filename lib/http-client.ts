import {
    ForbiddenError,
    InvalidRequestError,
    NotFoundError,
    ServerUnavailableError,
    UnauthorizedError,
    UnknownServerError
} from 'src/common/lib/api';
import { Session } from 'src/common/lib/session';

const convertHeaders = (headers?: { [index: string]: any; }): Headers => {
    const headersObject: Headers = new Headers();

    if(headers) {
        Object.keys(headers).forEach(key => {
            headersObject!.append(key, headers[key]);
        });
    }

    if(Session.sessionKey) {
        headersObject.append('Authorization', `Bearer ${Session.sessionKey}`);
    }

    return headersObject;
};

let renewingToken = false; //Avoids to ask the renew token again if the token is being renewed

/**
 * Common result for HTTP requests from above, using {@link fetch}.
 * @param res The response from {@link fetch}
 */
const commonResult = async <T> (res: Response): Promise<T> => {
    if(res.ok) {
        //If the token has to be renewed, it will do it.
        if(res.headers.has('X-ShouldRenewToken') && !renewingToken) {
            renewingToken = true;
            get<{ token: string }>('renew_token')
                .then(({ token }) => { Session.renewToken(token); renewingToken = false; })
                .catch(e => { console.error(e); renewingToken = false; }); //TODO this should not happen
        }

        return await res.json();
    } else {
        if(res.status >= 500) {
            //Backend server is unavailable :(
            throw new ServerUnavailableError({
                detail: await res.text(),
                status: res.status,
                title: res.statusText,
                type: res.status,
            });
        }

        if(res.headers.has('Content-Type') && res.headers.get('Content-Type')!.indexOf('json')) {
            const errorRaw = await res.json();
            if(res.status === 400) {
                throw new InvalidRequestError(errorRaw);
            } else if(res.status === 401) {
                throw new UnauthorizedError(errorRaw);
            } else if(res.status === 403) {
                throw new ForbiddenError(errorRaw);
            } else if(res.status === 404) {
                throw new NotFoundError(errorRaw);
            } else {
                throw new UnknownServerError(res, errorRaw);
            }
        } else {
            throw new UnknownServerError(res, await res.text());
        }
    }
};

const getPath = (path: string): string => !path.startsWith('http') && !path.startsWith('/') ? `/app-api/${path}` : path;

/**
 * Makes a `GET` request to the API server. It won't work for other backends, only for the specific Python one (made by
 * Antonio Ángel) or other servers that act like that. For other kind of requests, you can use {@link fetch} directly or
 * another library. If there's any error in the request, it will throw the plain object itself or one of the known errors.
 * @param path Path to the request. If relative (like `usuario`, it will use the route to the Antonio API's).
 * @param query If needed, adds a query string to the URL.
 * @param headers If needed, adds custom headers to the request.
 * @returns The response of the request in json format or an error if detected.
 * @see InvalidRequestError Invalid request error (400)
 * @see UnauthorizedError Unauthorized error (401)
 * @see ForbiddenError Forbidden error (403)
 * @see NotFoundError Not found error (404)
 * @see ServerUnavailableError Internal server error or unavailable (5XX)
 * @example
 * try {
 *   const result = await get<SelfUsuario>('usuario');
 *   const moreResults = await get<DevicesList>('dispositivos', { get_all_items: true });
 * } catch(e) { ... }
 */
export const get = async <T> (path: string, query?: { [index: string]: any }, headers?: { [index: string]: any; }): Promise<T> => {
    const queryStr = Object.keys(query || {})
        .filter(key => query![key] !== undefined && query![key] !== null)
        .map(key => `${encodeURIComponent(key)}=${encodeURIComponent(query![key])}`).join('&');
    const res = await fetch(getPath(path) + (query ? '?' + queryStr : ''), {
        headers: convertHeaders(headers)
    });

    return await commonResult<T>(res);
};

/**
 * Makes a `POST` request to the API server. It won't work for other backends, only for the specific Python one (made by
 * Antonio Ángel) or other servers that act like that. For other kind of requests, you can use {@link fetch} directly or
 * another library. If there's any error in the request, it will throw the plain object itself or one of the known errors.
 * @param path Path to the request. If relative (like `usuario`, it will use the route to the Antonio API's).
 * @param data Data to send
 * @param headers If needed, adds custom headers to the request.
 * @returns The response of the request in json format or an error if detected.
 * @see InvalidRequestError Invalid request error (400)
 * @see UnauthorizedError Unauthorized error (401)
 * @see ForbiddenError Forbidden error (403)
 * @see NotFoundError Not found error (404)
 * @see ServerUnavailableError Internal server error or unavailable (5XX)
 * @example
 * try {
 *   const result = await post<{ token: string }, { username: string, password: string }>('login', { username, password });
 * } catch(e) { ... }
 */
export const post = async <ReturnType, PostType> (path: string, data: PostType, headers?: { [index: string]: any; }): Promise<ReturnType> => {
    const headersObj = convertHeaders(headers) || new Headers();
    headersObj.append('Content-Type', 'application/json');
    const res = await fetch(getPath(path), {
        body: JSON.stringify(data),
        headers: headersObj,
        method: 'POST',
    });

    return await commonResult<ReturnType>(res);
};

/**
 * Makes a `PUT` request to the API server. It won't work for other backends, only for the specific Python one (made by
 * Antonio Ángel) or other servers that act like that. For other kind of requests, you can use {@link fetch} directly or
 * another library. If there's any error in the request, it will throw the plain object itself or one of the known errors.
 * @param path Path to the request. If relative (like `usuario`, it will use the route to the Antonio API's).
 * @param data Data to send
 * @param headers If needed, adds custom headers to the request.
 * @returns The response of the request in json format or an error if detected.
 * @see InvalidRequestError Invalid request error (400)
 * @see UnauthorizedError Unauthorized error (401)
 * @see ForbiddenError Forbidden error (403)
 * @see NotFoundError Not found error (404)
 * @see ServerUnavailableError Internal server error or unavailable (5XX)
 * @example
 * try {
 *   const result = await put<{ done: boolean, data: any }, { someData: any }>('collection', { someData });
 * } catch(e) { ... }
 */
export const put = async <ReturnType, PutType> (path: string, data: PutType, headers?: { [index: string]: any; }): Promise<ReturnType> => {
    const headersObj = convertHeaders(headers) || new Headers();
    headersObj.append('Content-Type', 'application/json');
    const res = await fetch(getPath(path), {
        body: JSON.stringify(data),
        headers: headersObj,
        method: 'PUT',
    });

    return await commonResult<ReturnType>(res);
};

/**
 * Makes a `PATCH` request to the API server. It won't work for other backends, only for the specific Python one (made by
 * Antonio Ángel) or other servers that act like that. For other kind of requests, you can use {@link fetch} directly or
 * another library. If there's any error in the request, it will throw the plain object itself or one of the known errors.
 * @param path Path to the request. If relative (like `usuario`, it will use the route to the Antonio API's).
 * @param data Partial data
 * @param headers If needed, adds custom headers to the request.
 * @returns The response of the request in json format or an error if detected.
 * @see InvalidRequestError Invalid request error (400)
 * @see UnauthorizedError Unauthorized error (401)
 * @see ForbiddenError Forbidden error (403)
 * @see NotFoundError Not found error (404)
 * @see ServerUnavailableError Internal server error or unavailable (5XX)
 * @example
 * try {
 *   const result = await post<{ done: boolean, data: any }, { one: any, two: any, three: any }>('collection/3', { two: 'yes' });
 * } catch(e) { ... }
 */
export const patch = async <ReturnType, PutType> (path: string, data: Partial<PutType>, headers?: { [index: string]: any; }): Promise<ReturnType> => {
    const headersObj = convertHeaders(headers) || new Headers();
    headersObj.append('Content-Type', 'application/json');
    const res = await fetch(getPath(path), {
        body: JSON.stringify(data),
        headers: headersObj,
        method: 'PATCH',
    });

    return await commonResult<ReturnType>(res);
};

/**
 * Makes a `DELETE` request to the API server. It won't work for other backends, only for the specific Python one (made by
 * Antonio Ángel) or other servers that act like that. For other kind of requests, you can use {@link fetch} directly or
 * another library. If there's any error in the request, it will throw the plain object itself or one of the known errors.
 * @param path Path to the request. If relative (like `usuario`, it will use the route to the Antonio API's).
 * @param headers If needed, adds custom headers to the request.
 * @returns The response of the request in json format or an error if detected.
 * @see InvalidRequestError Invalid request error (400)
 * @see UnauthorizedError Unauthorized error (401)
 * @see ForbiddenError Forbidden error (403)
 * @see NotFoundError Not found error (404)
 * @see ServerUnavailableError Internal server error or unavailable (5XX)
 * @example
 * try {
 *   const result = await del<{ done: boolean, data: any }>('collection/7');
 * } catch(e) { ... }
 */
export const del = async <T> (path: string, headers?: { [index: string]: any; }): Promise<T> => {
    const res = await fetch(getPath(path), {
        headers: convertHeaders(headers),
        method: 'DELETE',
    });

    return await commonResult<T>(res);
};

export const ws = (path: string): WebSocket => new WebSocket(`ws://localhost:3000${getPath(path)}`);
