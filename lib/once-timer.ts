/**
 * Class that implements the pattern "set a timeout, clearing the before if set"
 */
export class OnceTimer {

    private t: number | null = null;

    constructor(private func: () => void) {}

    /**
     * Sets the timeout, clearing the current one if there's any. Optionally, you can change the callback (for this one
     * call).
     * @param time Time in milliseconds
     * @param func Optional callback to call when time is out
     */
    public setTimeout(time: number, func?: () => void) {
        if(this.t) {
            clearTimeout(this.t);
        }

        this.t = setTimeout(() => {
            this.t = null;
            if(func) {
                func();
            } else {
                this.func();
            }
        }, time) as any;
    }

}