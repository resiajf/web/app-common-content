import {
    ComputerIssue,
    ComputerIssueState,
    ListComputerIssues,
    NewComputerIssue
} from 'src/common/lib/api/computer-issues/interfaces';
import { get, post } from '../../http-client';

export class ComputerIssues {

    public static async getList(page: number = 1, opts: { fromDate?: Date, toDate?: Date, getAllItems?: boolean }) {
        return await get<ListComputerIssues>('partes-informatica', {
            from_date: opts.fromDate ? opts.fromDate.toISOString() : undefined,
            get_all_items: opts.getAllItems,
            page,
            to_date: opts.toDate ? opts.toDate.toISOString() : undefined,
        });
    }

    public static async create(issue: NewComputerIssue) {
        return await post<ComputerIssue, NewComputerIssue>('partes-informatica', issue);
    }

    public static async getOne(id: number) {
        return await get<ComputerIssue>(`partes-informatica/${id}`);
    }

    public static async modifyState(issue: ComputerIssue | number, message: string, state?: ComputerIssueState) {
        const id = typeof issue === 'number' ? issue : issue.id_parte_informatica;
        return await post<
            ComputerIssue,
            { state?: ComputerIssueState, message: string }
        >(`partes-informatica/${id}/seguimiento`, { state, message });
    }

    public static async sendWaitingUserResponse(issue: ComputerIssue | number, message: string) {
        const id = typeof issue === 'number' ? issue : issue.id_parte_informatica;
        return await post<
            ComputerIssue,
            { razon: string }
        >(`partes-informatica/${id}/waiting-message`, { razon: message });
    }

    public static async discard(issue: ComputerIssue | number, reason: string) {
        const id = typeof issue === 'number' ? issue : issue.id_parte_informatica;
        return await post<ComputerIssue, { razon: string }>(`partes-informatica/${id}/discard`, { razon: reason });
    }

}
