import { ErrorBase, ErrorBaseType } from 'src/common/lib/api/errors/error.interface';

export class InvalidRequestError implements ErrorBase<'InvalidRequest'> {
    public readonly title: string;
    public readonly type: number;
    public readonly detail: string;
    public readonly kind = 'InvalidRequest';

    public constructor(obj: ErrorBaseType) {
        this.type = Number(obj.type) || -1;
        this.title = obj.title || '';
        this.detail = obj.detail || '';
    }

    public get translatedMessageKey(): string {
        switch(this.type) {
            case 350: return 'errors.invalid.350';
            case 459: return 'errors.invalid.459';
            case 460: return 'errors.invalid.460';
            case 551: return 'errors.invalid.551';
            default:
                return 'errors.invalid.default';
        }
    }

}