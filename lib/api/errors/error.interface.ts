export type ErrorBaseType = { type: string, title: string, detail: string, status: number } | any;

export interface ErrorBase<T extends string> {
    /**
     * A numberic code that represents the error
     */
    readonly type: number;

    /**
     * A developer readable short message of the error
     */
    readonly title: string;

    /**
     * A developer readable message of the error
     */
    readonly detail: string;

    /**
     * If available, this will return the pagePermissionKey of a translation that represents the error for the rest of the humans
     */
    readonly translatedMessageKey: string;

    readonly kind: T;
}
