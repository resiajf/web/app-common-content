import { Permission, Rol } from 'src/common/lib/api/permissions/interfaces';

export interface User {
    niu: number;
    display_name: string;
    apartment: number | null;
    rol_id_rol: number;
}

export interface SelfUser {
    usuario: User;
    info_permisos: {
        rol: Rol;
        permisos: Permission[];
    };
    unread_notifications: number;
}
