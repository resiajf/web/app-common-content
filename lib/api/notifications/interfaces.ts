export interface Notification {
    id_notificacion: number;
    titulo: string;
    cuerpo: string | null;
    enlace: string | null;
    date_time: Date;
    read: boolean;
    usuario_niu: number;
}

export interface NotificationList {
    start: number;
    limit: number;
    count: number;
    pages: number;
    results: Notification[];
}

export interface NotificationConfigBase {
    enabled: boolean;
}

interface NotificationConfigTelegram1 extends NotificationConfigBase {
    chat_id: null;
    activation_code: string;
}

interface NotificationConfigTelegram2 extends NotificationConfigBase {
    chat_id: string;
}

export type NotificationConfigTelegram = NotificationConfigTelegram1 | NotificationConfigTelegram2;

export type NotificationConfigTelegramTest = {
    done: false;
    reason: 280 | 281;
} | { done: true };