import { EventEmitter } from 'events';

import { Session } from 'src/common/lib/session';

export class NewNotificationsChannel extends EventEmitter {

    constructor(private ws: WebSocket) {
        super();

        this.ws.onopen = () => {
            this.emit('open');
            this.ws.send(JSON.stringify({ token: Session.sessionKey! }));
        };

        this.ws.onclose = () => {
            this.emit('close');
        };

        this.ws.onmessage = (e) => {
            this.emit('notification', JSON.parse(e.data));
        };

        this.ws.onerror = (e) => {
            this.emit('error', e);
        };
    }

    public close() {
        this.ws.close();
    }

}
