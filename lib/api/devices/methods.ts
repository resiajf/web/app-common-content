import { Device, ListDevices } from 'src/common/lib/api/devices/interfaces';
import { del, get } from 'src/common/lib/http-client';

export class Devices {

    public static async getList(page: number = 1, all: boolean = false, mac?: string) {
        return await get<ListDevices>('dispositivos', { get_all_items: all, page, mac });
    }

    public static async getOne(id: Device | number) {
        if(typeof id !== 'number') {
            id = id.id_dispositivo;
        }
        return await get<Device>(`dispositivos/${id}`);
    }

    public static async removeOne(id: Device | number) {
        if(typeof id !== 'number') {
            id = id.id_dispositivo;
        }
        return await del<Device>(`dispositivos/${id}`);
    }

}