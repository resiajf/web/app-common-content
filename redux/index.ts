import { default as errors } from 'src/common/redux/errors/reducer';
import { ErrorsState } from 'src/common/redux/errors/state';
import { default as user } from 'src/common/redux/user/reducers';
import { User } from 'src/common/redux/user/state';

export const reducers = {
    errors,
    user,
};

export interface State {
    errors: ErrorsState;
    user: User;
}
