import { User } from 'src/common/redux/user/state';

export default (state: User, action: any): User => {
    switch(action.type) {
        case 'notifications:CHANGED_READ_STATUS':
        case 'notifications:DISCARDED': {
            if(action.read) {
                return {
                    ...state,
                    numNotifications: Math.max(0, (state.numNotifications || 1) - 1),
                };
            }
            return state;
        }

        case 'NEW_NOTIFICATION': {
            return {
                ...state,
                numNotifications: (state.numNotifications || 0) + 1,
            };
        }
    }

    return state || {
        apartment: NaN,
        displayName: '¿',
        modules: [],
        niu: NaN,
        pages: [],
        permissionsPerPage: {},
        rol: NaN,
    };
};
