import { Action } from 'redux';

import {
    ForbiddenError,
    InvalidRequestError,
    NotFoundError,
    ServerUnavailableError,
    UnauthorizedError
} from 'src/common/lib/api';
import { ErrorsState } from 'src/common/redux/errors/state';

export const FORBIDDEN_ERROR = 'error:FORBIDDEN_ERROR';
export const INVALID_ERROR = 'error:INVALID_ERROR';
export const NOT_FOUND_ERROR = 'error:NOT_FOUND_ERROR';
export const SERVER_UNAVAILABLE_ERROR = 'error:SERVER_UNAVAILABLE_ERROR';
export const UNAUTHORIZED_ERROR = 'error:UNAUTHORIZED_ERROR';
export const CLEAR_ERROR = 'error:CLEAR_ERROR';

export interface ErrorAction <T, S> extends Action<S> {
    error: T;
}

type ForbiddenErrorAction = ErrorAction<ForbiddenError, typeof FORBIDDEN_ERROR>;
type InvalidErrorAction = ErrorAction<InvalidRequestError, typeof INVALID_ERROR>;
type NotFoundErrorAction = ErrorAction<NotFoundError, typeof NOT_FOUND_ERROR>;
type ServerUnavailableErrorAction = ErrorAction<ServerUnavailableError, typeof SERVER_UNAVAILABLE_ERROR>;
type UnauthorizedErrorAction = ErrorAction<UnauthorizedError, typeof UNAUTHORIZED_ERROR>;
interface ClearErrorAction extends Action<typeof CLEAR_ERROR> {
    what?: keyof ErrorsState;
}

export type ErrorsActions = ForbiddenErrorAction | InvalidErrorAction | NotFoundErrorAction |
    ServerUnavailableErrorAction | UnauthorizedErrorAction | ClearErrorAction;

export const forbiddenError = (error: ForbiddenError): ForbiddenErrorAction => ({
    error,
    type: FORBIDDEN_ERROR,
});

export const invalidError = (error: InvalidRequestError): InvalidErrorAction => ({
    error,
    type: INVALID_ERROR,
});

export const notFoundError = (error: NotFoundError): NotFoundErrorAction => ({
    error,
    type: NOT_FOUND_ERROR,
});

export const serverUnavailableError = (error: ServerUnavailableError): ServerUnavailableErrorAction => ({
    error,
    type: SERVER_UNAVAILABLE_ERROR,
});

export const unauthorizedError = (error: UnauthorizedError): UnauthorizedErrorAction => ({
    error,
    type: UNAUTHORIZED_ERROR,
});

export const clearError = (what?: keyof ErrorsState): ClearErrorAction => ({
    type: CLEAR_ERROR,
    what,
});
