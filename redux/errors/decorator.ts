import { Dispatch } from 'redux';

import { ForbiddenError, InvalidRequestError, NotFoundError, ServerUnavailableError, UnauthorizedError } from 'src/common/lib/api';
import {
    forbiddenError,
    invalidError,
    notFoundError,
    serverUnavailableError,
    unauthorizedError
} from 'src/common/redux/errors/actions';

/**
 * Decorator that catches all the errors generated from the http client (`lib/http-client.ts`) and dispatches them as
 * a redux actions into the `errors` state.
 * @param func Function to decorate
 * @example
 * import { catchErrors as f } from 'src/common/redux/errors/decorator';
 * export const deleteDevice = (theDevice: Device) => f(async (dispatch: Dispatch) => {
 *     dispatch({ type: UPDATING });
 *     const device = await Devices.removeOne(theDevice);
 *     dispatch({
 *         device,
 *         type: REMOVE_DEVICE,
 *     });
 * });
 */
export const catchErrors = <T> (func: (dispatch: Dispatch) => Promise<T>) => (dispatch: Dispatch) => {
    func(dispatch).catch((e) => {
        if(e instanceof UnauthorizedError) {
            dispatch(unauthorizedError(e));
        } else if(e instanceof ForbiddenError) {
            dispatch(forbiddenError(e));
        } else if(e instanceof InvalidRequestError) {
            dispatch(invalidError(e));
        } else if(e instanceof NotFoundError) {
            dispatch(notFoundError(e));
        } else if(e instanceof ServerUnavailableError) {
            dispatch(serverUnavailableError(e));
        } else {
            //Rethrow error
            throw e;
        }
    });
};
