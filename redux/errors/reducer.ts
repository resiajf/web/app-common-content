import {
    CLEAR_ERROR,
    ErrorsActions,
    FORBIDDEN_ERROR,
    INVALID_ERROR,
    NOT_FOUND_ERROR, SERVER_UNAVAILABLE_ERROR,
    UNAUTHORIZED_ERROR
} from 'src/common/redux/errors/actions';
import { ErrorsState } from 'src/common/redux/errors/state';

export default (errors: ErrorsState, action: ErrorsActions): ErrorsState => {
    switch(action.type) {
        case FORBIDDEN_ERROR: {
            return {
                ...errors,
                forbidden: action.error,
            };
        }

        case INVALID_ERROR: {
            return {
                ...errors,
                invalid: action.error,
            };
        }

        case NOT_FOUND_ERROR: {
            return {
                ...errors,
                notFound: action.error,
            };
        }

        case SERVER_UNAVAILABLE_ERROR: {
            return {
                ...errors,
                serverUnavailable: action.error,
            };
        }

        case UNAUTHORIZED_ERROR: {
            return {
                ...errors,
                unauthorized: action.error,
            };
        }

        case CLEAR_ERROR: {
            if(action.what === undefined) {
                return {
                    forbidden: null,
                    invalid: null,
                    notFound: null,
                    serverUnavailable: null,
                    unauthorized: null,
                };
            } else {
                return {
                    ...errors,
                    [action.what]: null,
                };
            }
        }

        default: {
            return errors || {
                forbidden: null,
                invalid: null,
                notFound: null,
                serverUnavailable: null,
                unauthorized: null,
            };
        }
    }
};
