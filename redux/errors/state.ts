import { ForbiddenError, InvalidRequestError, NotFoundError, ServerUnavailableError, UnauthorizedError } from 'src/common/lib/api';

export interface ErrorsState {
    forbidden: ForbiddenError | null;
    invalid: InvalidRequestError | null;
    notFound: NotFoundError | null;
    serverUnavailable: ServerUnavailableError | null;
    unauthorized: UnauthorizedError | null;
}